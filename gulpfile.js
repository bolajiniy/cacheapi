var gulp = require('gulp');
var nodemon = require('gulp-nodemon');
var ts = require("gulp-typescript");
var tsProject = ts.createProject("tsconfig.json");


gulp.task('typescript', function () {

    return tsProject.src()
        .pipe(tsProject())
        .js.pipe(gulp.dest("www/js"));

});

gulp.task('default', ['typescript'], function () {

    nodemon({
        script: './www/js/app.js',
        ext: 'ts,ejs',
        watch: ['www/views', 'src'],
        verbose: true,
        tasks: ['typescript'],
        env: {
            PORT: 8001
        },
        delay: 2,
        ignore: ['./node_modules/**', './www/js/**']
    }).on('restart', function () {
        console.log('Re-Starting.....');
    });
});