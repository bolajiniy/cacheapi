import { Server } from './sever';


/**
 * start the server;
 */

let debug = require("debug")("express:server");
let server = new Server();
let httpServer = server.getServer();
let httpPort = server.httpPort;


//add error handler
httpServer.on("error", onError);

//start listening on port
httpServer.on("listening", onListening);

//listen on provided ports
httpServer.listen(httpPort);

/**
 * Event listener for HTTP server "error" event.
 */
function onError(error) {

    debug('Error occurred .... : ', httpPort);
    if (error.syscall !== "listen") {
        throw error;
    }

    var bind = typeof httpPort === "string"
        ? "Pipe " + httpPort
        : "Port " + httpPort;

    // handle specific listen errors with friendly messages
    switch (error.code) {
        case "EACCES":
            debug(bind + " requires elevated privileges");
            process.exit(1);
            break;
        case "EADDRINUSE":
            debug(bind + " is already in use");
            process.exit(1);
            break;
        default:
            throw error;
    }
}

/**
 * Event listener for HTTP server "listening" event.
 */
function onListening() {

    var addr = httpServer.address();
    debug('onListening.... : ', httpPort);
    debug('onListening.... : ', addr);

    var bind = typeof addr === "string"
        ? "pipe " + addr
        : "port " + addr.port;
    debug("Listening on " + bind);
}
