import { Request, NextFunction, Response } from "express";
import { CacheRepository } from "../repositories";
import { CacheViewModel } from "../viewmodels/cache.viewmodel";
import { ICacheModel } from "../models";
import * as crypto from "crypto";
import { Constants } from "../constants";

import * as bunyan from 'bunyan';
var log = bunyan.createLogger({ name: "cacheapi" });

/** Handle all cache business logics
 * @class CacheController
 * 
*/
export class CacheController {



    static deleteAll(req: Request, res: Response, next: NextFunction) {

        try {


            let cacheRepo = new CacheRepository();

            cacheRepo.delete({}, function (error, result) {

                if (error) {
                    res.status(500).send('Request failed');
                } else {
                    res.status(202).send('deleted');
                }

            });

        }
        catch (ex) {
            res.status(500).send('Request failed');
        }

    }

    static delete(req: Request, res: Response, next: NextFunction) {

        try {

            let key: string = req.params.key;

            if (key) {
                let cacheRepo = new CacheRepository();


                cacheRepo.delete({ key: key }, function (error, result) {

                    if (error) {
                        res.status(500).send('Request failed');
                    } else {
                        res.status(201).send('deleted');
                    }

                });
            }
            else {
                res.status(400).send('Invalid request');
            }

        }
        catch (ex) {
            res.status(500).send('Request failed');
        }

    }



    static async post(req: Request, res: Response, next: NextFunction) {

        try {

            let body = <CacheViewModel>req.body;

            if (body && body.key) {

                let cacheRepo = new CacheRepository();

                //get cache existing cache with same key or expire or laste modified
                let cache = await this.getCache(body.key);

                cache.key = body.key;
                cache.content = this.getRandomString();

                if (cache._id) {

                    cacheRepo.update({ _id: cache._id }, cache, function (error, result) {

                        if (error || !result) {
                            res.status(500).send(error);
                        } else {

                            let retVal = new CacheViewModel();
                            retVal.key = cache.key;
                            retVal.value = cache.content;

                            res.status(201).send(retVal);
                        }

                    });
                }
                else {
                    cacheRepo.create(cache, function (error, result) {

                        if (error || !result) {
                            res.status(500).send(error);
                        } else {

                            let retVal = new CacheViewModel();
                            retVal.key = cache.key;
                            retVal.value = cache.content;

                            res.status(201).send(retVal);
                        }

                    });
                }

            }
            else {
                res.status(400).send('Invalid request');
            }

        }
        catch (ex) {
            log.error(ex);
            res.status(500).send('Request failed');
        }


    }

    static put(req: Request, res: Response, next: NextFunction) {
        this.post(req, res, next);
    }

    static get(req: Request, res: Response, next: NextFunction) {

        try {

            let cacheRepo = new CacheRepository();
            let now = new Date();

            //{ expireOn: { $lt: now } }
            cacheRepo.retrieve({ expireOn: { $lte: now } }, function (error, result) {

                if (error) {
                    res.status(500).send('Request failed');
                }
                else {
                    var viewModel = result.map(cache => {

                        let retVal = new CacheViewModel();
                        retVal.key = cache.key;
                        retVal.value = cache.content;

                        return retVal;
                    });

                    res.status(200).send(viewModel);
                }

            });


        }
        catch (ex) {
            res.status(500).send('Request failed');
        }

    }


    static getByKey(req: Request, res: Response, next: NextFunction) {

        try {

            let key: string = req.params.key;

            if (key) {


                let cacheRepo = new CacheRepository();
                let now = new Date();

                // expireOn: { $lt: now }
                cacheRepo.findOne({ key: key, expireOn: { $lte: now } }, function (error, cache) {

                    if (error) {
                        res.status(500).send('Request failed');
                    }
                    else if (!cache) {
                        log.info('Cache miss');
                        req.body = { key: key };
                        CacheController.post(req, res, next);

                    }
                    else {
                        log.info('Cache found');
                        let retVal = new CacheViewModel();
                        retVal.key = cache.key;
                        retVal.value = cache.content;

                        res.status(200).send(retVal);
                    }

                });
            }
            else {

                res.status(404).send('Invalid request');
            }


        }
        catch (ex) {
            log.error(ex);
            res.status(500).send('Request failed');
        }
    }

    /**
     * Generate random string
     * @class CacheController
     * @method getRandomString
     * @returns {string} returns string generated
     */
    private static getRandomString(): string {
        return crypto.randomBytes(50).toString('hex');
    }

    /**
     * Returns new or existing cache 
     * @param key 
     */
    private static async getCache(key: string): Promise<ICacheModel> {

        let retVal: ICacheModel = null;
        let cacheRepo = new CacheRepository();

        //does the key exists
        retVal = await cacheRepo.findOne({ key: key }).exec();

        if (!retVal) {


            //get any expired one
            let now = new Date();
            retVal = await cacheRepo.findOne({ expireOn: { $gte: now } }).exec();

            if (!retVal) {

                //get last modified
                let caches = await cacheRepo.retrieve({}).sort({ modifiedAt: 1 }).skip(Constants.CACHE_LIMIT - 1).exec();

                if (caches && caches.length > 0) {
                    retVal = caches[0];

                    try {
                        //todo : delete overflows with job
                        for (let i = 1; i < caches.length; i++) {
                            caches[i].remove();
                        }
                    }
                    catch (ex) {
                        log.error(ex);
                    }
                }

            }
        }

        if (!retVal) {

            retVal = {} as ICacheModel;
        }


        let now = new Date();

        if (!retVal.createdAt) {
            retVal.createdAt = now;
            //retVal._id = cacheRepo.getObjectId();
        }
        retVal.modifiedAt = now;

        //update or reset Time To Live (TTL)
        let ttl = new Date();
        ttl.setHours(now.getHours() + Constants.CACHE_LIFE_SPAN);
        //ttl.setMinutes(now.getMinutes() + Constants.CACHE_LIFE_SPAN);

        retVal.expireOn = ttl;

        //log.info(retVal);
        return retVal;

    }

}