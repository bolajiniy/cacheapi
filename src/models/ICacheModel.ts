import { Document } from "mongoose";


export interface ICacheModel extends Document {
    key: string;
    content: string;
    createdAt: Date;
    modifiedAt: Date;
    expireOn: Date;
}