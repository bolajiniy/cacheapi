import * as mongoose from 'mongoose';
import { Constants } from '../constants';
import { DocumentQuery, Query } from 'mongoose';
import { ObjectId } from 'bson';

export abstract class RepositoryBase<T extends mongoose.Document>  {

    private model: mongoose.Model<T>;

    constructor(name: string, schema?: mongoose.Schema, collection?: string) {

        //init model 
        mongoose.connect(Constants.DB);
        this.model = mongoose.model<T>(name, schema, collection);

    }

    updateOrInsert(conditions: any, item: T, callback: (error: any, result: any) => void): Query<any> {

        return this.model.update(conditions, item, { upsert: true }, callback);
    }

    create(item: T, callback: (error: any, result: any) => void): Promise<T> {
        return this.model.create(item, callback);
    }

    retrieve(conditions: any, callback?: (err: any, res: T[]) => void): DocumentQuery<T[], T> {
        return this.model.find(conditions, callback)
    }

    update(conditions: any, item: T, callback: (error: any, result: any) => void): Query<any> {
        return this.model.update(conditions, item, callback);
    }

    delete(conditions: any, callback?: (error: any, result: any) => void): Query<void> {
        return this.model.remove(conditions, (err) => callback(err, null));
    }

    findById(_id: string, callback?: (error: any, result: T) => void): DocumentQuery<T | null, T> {
        return this.model.findById(_id, callback);
    }

    findOne(conditions: any, callback?: (error: any, result: T) => void): DocumentQuery<T | null, T> {
        return this.model.findOne(conditions, callback);
    }


    private toObjectId(_id: string): mongoose.Types.ObjectId {
        return mongoose.Types.ObjectId.createFromHexString(_id)
    }

    getObjectId(): ObjectId {
        return mongoose.Types.ObjectId(new Date().toDateString());
    }

}