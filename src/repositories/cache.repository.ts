import { RepositoryBase } from './index';
import { ICacheModel } from '../models/index';
import { CacheSchema } from '../schemas/index';

export class CacheRepository extends RepositoryBase<ICacheModel> {
    constructor() {
        super("cache", CacheSchema);
    }
}