import { NextFunction, Request, Response } from "express";
import * as express from 'express';
import { Router } from "express-serve-static-core";
import { CacheController } from "../controllers";




export class CacheRoute {

    static get Routes(): Router {

        var router = express.Router();

        router.route('/')
            .get(function (req, res, next) {
                CacheController.get(req, res, next);
            })
            .post(function (req, res, next) {
                CacheController.post(req, res, next);
            })
            .delete(function (req, res, next) {
                CacheController.deleteAll(req, res, next);
            });

        router.route('/:key')
            .get(function (req, res, next) {
                CacheController.getByKey(req, res, next);
            })
            .delete(function (req, res, next) {
                CacheController.delete(req, res, next);
            })
            .put(function (req, res, next) {
                CacheController.put(req, res, next)
            });

        return router;
    }

}