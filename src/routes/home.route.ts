import { NextFunction, Request, Response } from "express";
import * as express from 'express';
import { Router } from "express-serve-static-core";

export class HomeRoute {

    static get Routes(): Router {

        var router = express.Router();

        router.route('/')
            .get(function (req, res) {
                res.render('index', { title: 'Cache API', message: 'You are most welcome!' });
            });

        return router;

    }

}