import { Schema } from "mongoose";
import { ICacheModel } from "../models/index"
import { Constants } from "../constants";


export let CacheSchema: Schema = new Schema({
    createdAt: Date,
    modifiedAt: Date,
    expireOn: Date,
    key: {
        type: String,
        unique: true
    },
    content: String,

});

//remove login from here
/**
 * intercept all adding and updating actions. Update the cache date properties accordingly
 
CacheSchema.pre("save", function (next) {

    if (this._doc) {

        //get strongly typed object
        let item = <ICacheModel>this._doc;

        //check null
        if (item) {

            let now = new Date();

            //if new record set created time
            if (!item.createdAt) {
                item.createdAt = now;
            }
            item.modifiedAt = now;

            //update or reset Time To Live (TTL)
            let ttl = new Date();
            //ttl.setHours(now.getHours() + Constants.CACHE_LIFE_SPAN);
            ttl.setMinutes(now.getMinutes() + Constants.CACHE_LIFE_SPAN);

            item.expireOn = ttl;
        }

    }

    next();
    return this;

});*/