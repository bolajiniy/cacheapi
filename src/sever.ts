import * as bodyParser from "body-parser";
//import * as cookieParser from "cookie-parser";
import * as express from "express";
import * as logger from "morgan";
import * as path from "path";
import * as errorHandler from "errorhandler";
import * as  http from "http";
import { HomeRoute, CacheRoute } from "./routes";
import { Constants } from "./constants";



/**
 * The server.
 *
 * @class Server
 */
export class Server {


    public app: express.Application;


    /**
     * Constructor, configure application and routes 
     *
     * @class Server
     * @constructor
     */
    constructor() {

        //create expressjs application
        this.app = express();
        this.app.set("port", this.httpPort);

        //configure application
        this.config();

        //add routes
        this.routes();

    }


    /**
   * create and return http.Server object
   *
   * @class Server
   * @method bootstrap 
   * @return {http.Server}
   */
    public getServer(): http.Server {

        //create http server 
        return http.createServer(this.app);

    }


    /**
     * Configure application
     *
     * @class Server
     * @method config
     */
    public config() {
        //add static paths : static css, image etc
        this.app.use(express.static(path.join(__dirname, "../public")));

        //configure pug
        this.app.set("views", path.join(__dirname, "../views"));
        this.app.set("view engine", "ejs");

        //mount logger
        this.app.use(logger("dev"));

        //mount json form parser
        this.app.use(bodyParser.json());

        //mount query string parser
        this.app.use(bodyParser.urlencoded({
            extended: true
        }));


        // catch 404 and forward to error handler
        this.app.use(function (err: any, req: express.Request, res: express.Response, next: express.NextFunction) {
            err.status = 404;
            next(err);
        });


        //error handling 
        this.app.use(errorHandler());
    }

    /**
     * Create and return Router.
     *

     * @class Server
     * @method config
     * @return void
     */
    private routes() {

        this.app.use('/', HomeRoute.Routes);
        this.app.use('/api/caches', CacheRoute.Routes);
    }


    /**
     * Get the application http port number
     */
    get httpPort(): number {

        var port = parseInt(process.env.PORT, 10);

        if (isNaN(port) || port < 0) {
            return Constants.PORT;
        }
        return port;
    }


}