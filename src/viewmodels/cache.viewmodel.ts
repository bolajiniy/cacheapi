
/** To control data recieve and send to the client; DTO
 * @class
 */
export class CacheViewModel {
    key: string;
    value: string;
}