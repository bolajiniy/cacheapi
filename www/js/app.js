"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var sever_1 = require("./sever");
var debug = require("debug")("express:server");
var server = new sever_1.Server();
var httpServer = server.getServer();
var httpPort = server.httpPort;
httpServer.on("error", onError);
httpServer.on("listening", onListening);
httpServer.listen(httpPort);
function onError(error) {
    debug('Error occurred .... : ', httpPort);
    if (error.syscall !== "listen") {
        throw error;
    }
    var bind = typeof httpPort === "string"
        ? "Pipe " + httpPort
        : "Port " + httpPort;
    switch (error.code) {
        case "EACCES":
            debug(bind + " requires elevated privileges");
            process.exit(1);
            break;
        case "EADDRINUSE":
            debug(bind + " is already in use");
            process.exit(1);
            break;
        default:
            throw error;
    }
}
function onListening() {
    var addr = httpServer.address();
    debug('onListening.... : ', httpPort);
    debug('onListening.... : ', addr);
    var bind = typeof addr === "string"
        ? "pipe " + addr
        : "port " + addr.port;
    debug("Listening on " + bind);
}
