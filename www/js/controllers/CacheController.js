"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var repositories_1 = require("../repositories");
var cache_viewmodel_1 = require("../viewmodels/cache.viewmodel");
var crypto = require("crypto");
var constants_1 = require("../constants");
var bunyan = require("bunyan");
var log = bunyan.createLogger({ name: "cacheapi" });
var CacheController = (function () {
    function CacheController() {
    }
    CacheController.deleteAll = function (req, res, next) {
        try {
            var cacheRepo = new repositories_1.CacheRepository();
            cacheRepo.delete({}, function (error, result) {
                if (error) {
                    res.status(500).send('Request failed');
                }
                else {
                    res.status(202).send('deleted');
                }
            });
        }
        catch (ex) {
            res.status(500).send('Request failed');
        }
    };
    CacheController.delete = function (req, res, next) {
        try {
            var key = req.params.key;
            if (key) {
                var cacheRepo = new repositories_1.CacheRepository();
                cacheRepo.delete({ key: key }, function (error, result) {
                    if (error) {
                        res.status(500).send('Request failed');
                    }
                    else {
                        res.status(201).send('deleted');
                    }
                });
            }
            else {
                res.status(400).send('Invalid request');
            }
        }
        catch (ex) {
            res.status(500).send('Request failed');
        }
    };
    CacheController.post = function (req, res, next) {
        return __awaiter(this, void 0, void 0, function () {
            var body, cacheRepo, cache_1, ex_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 4, , 5]);
                        body = req.body;
                        if (!(body && body.key)) return [3, 2];
                        cacheRepo = new repositories_1.CacheRepository();
                        return [4, this.getCache(body.key)];
                    case 1:
                        cache_1 = _a.sent();
                        cache_1.key = body.key;
                        cache_1.content = this.getRandomString();
                        if (cache_1._id) {
                            cacheRepo.update({ _id: cache_1._id }, cache_1, function (error, result) {
                                if (error || !result) {
                                    res.status(500).send(error);
                                }
                                else {
                                    var retVal = new cache_viewmodel_1.CacheViewModel();
                                    retVal.key = cache_1.key;
                                    retVal.value = cache_1.content;
                                    res.status(201).send(retVal);
                                }
                            });
                        }
                        else {
                            cacheRepo.create(cache_1, function (error, result) {
                                if (error || !result) {
                                    res.status(500).send(error);
                                }
                                else {
                                    var retVal = new cache_viewmodel_1.CacheViewModel();
                                    retVal.key = cache_1.key;
                                    retVal.value = cache_1.content;
                                    res.status(201).send(retVal);
                                }
                            });
                        }
                        return [3, 3];
                    case 2:
                        res.status(400).send('Invalid request');
                        _a.label = 3;
                    case 3: return [3, 5];
                    case 4:
                        ex_1 = _a.sent();
                        log.error(ex_1);
                        res.status(500).send('Request failed');
                        return [3, 5];
                    case 5: return [2];
                }
            });
        });
    };
    CacheController.put = function (req, res, next) {
        this.post(req, res, next);
    };
    CacheController.get = function (req, res, next) {
        try {
            var cacheRepo = new repositories_1.CacheRepository();
            var now = new Date();
            cacheRepo.retrieve({ expireOn: { $lte: now } }, function (error, result) {
                if (error) {
                    res.status(500).send('Request failed');
                }
                else {
                    var viewModel = result.map(function (cache) {
                        var retVal = new cache_viewmodel_1.CacheViewModel();
                        retVal.key = cache.key;
                        retVal.value = cache.content;
                        return retVal;
                    });
                    res.status(200).send(viewModel);
                }
            });
        }
        catch (ex) {
            res.status(500).send('Request failed');
        }
    };
    CacheController.getByKey = function (req, res, next) {
        try {
            var key_1 = req.params.key;
            if (key_1) {
                var cacheRepo = new repositories_1.CacheRepository();
                var now = new Date();
                cacheRepo.findOne({ key: key_1, expireOn: { $lte: now } }, function (error, cache) {
                    if (error) {
                        res.status(500).send('Request failed');
                    }
                    else if (!cache) {
                        log.info('Cache miss');
                        req.body = { key: key_1 };
                        CacheController.post(req, res, next);
                    }
                    else {
                        log.info('Cache found');
                        var retVal = new cache_viewmodel_1.CacheViewModel();
                        retVal.key = cache.key;
                        retVal.value = cache.content;
                        res.status(200).send(retVal);
                    }
                });
            }
            else {
                res.status(404).send('Invalid request');
            }
        }
        catch (ex) {
            log.error(ex);
            res.status(500).send('Request failed');
        }
    };
    CacheController.getRandomString = function () {
        return crypto.randomBytes(50).toString('hex');
    };
    CacheController.getCache = function (key) {
        return __awaiter(this, void 0, void 0, function () {
            var retVal, cacheRepo, now_1, caches, i, now, ttl;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        retVal = null;
                        cacheRepo = new repositories_1.CacheRepository();
                        return [4, cacheRepo.findOne({ key: key }).exec()];
                    case 1:
                        retVal = _a.sent();
                        if (!!retVal) return [3, 4];
                        now_1 = new Date();
                        return [4, cacheRepo.findOne({ expireOn: { $gte: now_1 } }).exec()];
                    case 2:
                        retVal = _a.sent();
                        if (!!retVal) return [3, 4];
                        return [4, cacheRepo.retrieve({}).sort({ modifiedAt: 1 }).skip(constants_1.Constants.CACHE_LIMIT - 1).exec()];
                    case 3:
                        caches = _a.sent();
                        if (caches && caches.length > 0) {
                            retVal = caches[0];
                            try {
                                for (i = 1; i < caches.length; i++) {
                                    caches[i].remove();
                                }
                            }
                            catch (ex) {
                                log.error(ex);
                            }
                        }
                        _a.label = 4;
                    case 4:
                        if (!retVal) {
                            retVal = {};
                        }
                        now = new Date();
                        if (!retVal.createdAt) {
                            retVal.createdAt = now;
                        }
                        retVal.modifiedAt = now;
                        ttl = new Date();
                        ttl.setHours(now.getHours() + constants_1.Constants.CACHE_LIFE_SPAN);
                        retVal.expireOn = ttl;
                        return [2, retVal];
                }
            });
        });
    };
    return CacheController;
}());
exports.CacheController = CacheController;
