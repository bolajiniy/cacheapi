"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose = require("mongoose");
var constants_1 = require("../constants");
var RepositoryBase = (function () {
    function RepositoryBase(name, schema, collection) {
        mongoose.connect(constants_1.Constants.DB);
        this.model = mongoose.model(name, schema, collection);
    }
    RepositoryBase.prototype.updateOrInsert = function (conditions, item, callback) {
        return this.model.update(conditions, item, { upsert: true }, callback);
    };
    RepositoryBase.prototype.create = function (item, callback) {
        return this.model.create(item, callback);
    };
    RepositoryBase.prototype.retrieve = function (conditions, callback) {
        return this.model.find(conditions, callback);
    };
    RepositoryBase.prototype.update = function (conditions, item, callback) {
        return this.model.update(conditions, item, callback);
    };
    RepositoryBase.prototype.delete = function (conditions, callback) {
        return this.model.remove(conditions, function (err) { return callback(err, null); });
    };
    RepositoryBase.prototype.findById = function (_id, callback) {
        return this.model.findById(_id, callback);
    };
    RepositoryBase.prototype.findOne = function (conditions, callback) {
        return this.model.findOne(conditions, callback);
    };
    RepositoryBase.prototype.toObjectId = function (_id) {
        return mongoose.Types.ObjectId.createFromHexString(_id);
    };
    RepositoryBase.prototype.getObjectId = function () {
        return mongoose.Types.ObjectId(new Date().toDateString());
    };
    return RepositoryBase;
}());
exports.RepositoryBase = RepositoryBase;
