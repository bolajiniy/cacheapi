"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var controllers_1 = require("../controllers");
var CacheRoute = (function () {
    function CacheRoute() {
    }
    Object.defineProperty(CacheRoute, "Routes", {
        get: function () {
            var router = express.Router();
            router.route('/')
                .get(function (req, res, next) {
                controllers_1.CacheController.get(req, res, next);
            })
                .post(function (req, res, next) {
                controllers_1.CacheController.post(req, res, next);
            })
                .delete(function (req, res, next) {
                controllers_1.CacheController.deleteAll(req, res, next);
            });
            router.route('/:key')
                .get(function (req, res, next) {
                controllers_1.CacheController.getByKey(req, res, next);
            })
                .delete(function (req, res, next) {
                controllers_1.CacheController.delete(req, res, next);
            })
                .put(function (req, res, next) {
                controllers_1.CacheController.put(req, res, next);
            });
            return router;
        },
        enumerable: true,
        configurable: true
    });
    return CacheRoute;
}());
exports.CacheRoute = CacheRoute;
