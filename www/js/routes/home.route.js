"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var HomeRoute = (function () {
    function HomeRoute() {
    }
    Object.defineProperty(HomeRoute, "Routes", {
        get: function () {
            var router = express.Router();
            router.route('/')
                .get(function (req, res) {
                res.render('index', { title: 'Cache API', message: 'You are most welcome!' });
            });
            return router;
        },
        enumerable: true,
        configurable: true
    });
    return HomeRoute;
}());
exports.HomeRoute = HomeRoute;
