"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = require("mongoose");
exports.CacheSchema = new mongoose_1.Schema({
    createdAt: Date,
    modifiedAt: Date,
    expireOn: Date,
    key: {
        type: String,
        unique: true
    },
    content: String,
});
