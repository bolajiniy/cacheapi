"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var bodyParser = require("body-parser");
var express = require("express");
var logger = require("morgan");
var path = require("path");
var errorHandler = require("errorhandler");
var http = require("http");
var routes_1 = require("./routes");
var constants_1 = require("./constants");
var Server = (function () {
    function Server() {
        this.app = express();
        this.app.set("port", this.httpPort);
        this.config();
        this.routes();
    }
    Server.prototype.getServer = function () {
        return http.createServer(this.app);
    };
    Server.prototype.config = function () {
        this.app.use(express.static(path.join(__dirname, "../public")));
        this.app.set("views", path.join(__dirname, "../views"));
        this.app.set("view engine", "ejs");
        this.app.use(logger("dev"));
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({
            extended: true
        }));
        this.app.use(function (err, req, res, next) {
            err.status = 404;
            next(err);
        });
        this.app.use(errorHandler());
    };
    Server.prototype.routes = function () {
        this.app.use('/', routes_1.HomeRoute.Routes);
        this.app.use('/api/caches', routes_1.CacheRoute.Routes);
    };
    Object.defineProperty(Server.prototype, "httpPort", {
        get: function () {
            var port = parseInt(process.env.PORT, 10);
            if (isNaN(port) || port < 0) {
                return constants_1.Constants.PORT;
            }
            return port;
        },
        enumerable: true,
        configurable: true
    });
    return Server;
}());
exports.Server = Server;
